The sale, dismantling, and shipping of the London Bridge to Lake Havasu City, Arizona (not Atlanta) is a fascinating historical event that took place in the late 1960s. The bridge was sold to Robert P. McCulloch, an American entrepreneur and founder of Lake Havasu City.

To find contemporary newspaper coverage from that time period, you can use several resources:

1. **Online Newspaper Archives:**
   - **Newspapers.com:** This is a subscription-based service that offers access to millions of pages of historical newspapers.
   - **ProQuest Historical Newspapers:** Many libraries offer access to this database which includes archives from major newspapers like The New York Times, The Washington Post, and others.
   - **Google News Archive:** Google has digitized many old newspapers which can be accessed for free.

2. **Library Resources:**
   - Visit your local library or university library. They often have subscriptions to newspaper archives and databases.
   - Ask a librarian for assistance in finding microfilm or digital copies of newspapers from the late 1960s.

3. **British Newspaper Archives:**
   - The British Library's newspaper archive might have relevant articles about the sale and dismantling process as it would have been significant news in the UK at the time.

4. **Public Records and Historical Societies:**
   - Contact historical societies both in London and Lake Havasu City for any archived materials they might hold regarding this event.

5. **Digital Collections:**
   - Websites like Chronicling America (from the Library of Congress) provide access to historic American newspapers.

When searching these resources, use keywords such as "London Bridge sale 1968," "London Bridge dismantling," "Robert P. McCulloch London Bridge," and similar terms to narrow down your search results effectively.

If you need specific articles or more detailed guidance on how to navigate these resources, feel free to ask!

# Sat  1 Jun 09:21:11 CEST 2024 - find me some contemporary newspaper coverage of the sale, dismantling and shipping to Atlanta of London Bridge